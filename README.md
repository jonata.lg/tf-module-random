# tf-module-random

## Descrição

Este módulo define um recurso `random_string` no Terraform. O recurso `random_string` é utilizado para gerar uma string aleatória de comprimento especificado, sem caracteres especiais.

## Recursos

### random_string.main

- **Descrição**: Gera uma string aleatória de comprimento definido, sem incluir caracteres especiais.
- **Tipo de Recurso**: `random_string`

#### Argumentos

- **length** (Obrigatório)
  - Descrição: Especifica o comprimento da string aleatória.
  - Tipo: `number`
  - Valor: Variável `var.length`

- **special** (Opcional)
  - Descrição: Indica se a string gerada deve incluir caracteres especiais. Neste caso, está definido como `false`, ou seja, a string não incluirá caracteres especiais.
  - Tipo: `bool`
  - Valor: `false`

## Variáveis

### var.length

- **Descrição**: Define o comprimento da string aleatória que será gerada pelo recurso `random_string.main`.
- **Tipo**: `number`
- **Exemplo de Valor**: `10`

## Exemplos de Uso

```hcl
module "random_string_example" {
  source = "git@gitlab.com:jonata.lg/tf-module-random.git"
  length = 16
}
```

Neste exemplo, a variável `length` é definida com um valor de `16`. O recurso `random_string.main` irá gerar uma string aleatória de 16 caracteres, sem caracteres especiais.

## Considerações

- Certifique-se de definir a variável `length` conforme necessário para o seu uso específico.
- O recurso `random_string` é útil para gerar senhas, tokens ou quaisquer outras strings que precisem ser únicas e seguras.
- Como `special` está definido como `false`, a string gerada não conterá caracteres especiais, apenas letras e números.

---

Esta documentação fornece uma visão geral do recurso `random_string` definido no arquivo `main.tf`, detalhando seus argumentos, variáveis associadas e exemplos de uso.