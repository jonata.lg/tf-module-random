variable "length" {
  type = number
  description = "The length of the string desired."
}